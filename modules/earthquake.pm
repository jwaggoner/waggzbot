
package earthquake;
use strict;
use warnings;


my ($no_earthquake, $no_posix);


BEGIN {
    eval qq{
        use LWP::Simple qw();
		use XML::Feed;
		use HTTP::Request::Common;
		use Data::Dumper;
    };
    $no_earthquake++ if ($@);

    eval qq{
        use POSIX;
    };
    $no_posix++ if ($@);
}

sub earthquake_get($$)
{
    if($no_earthquake)
    {
        &main::status("Sorry, earthquake.pm requires LWP, and XML::Simple couldn't find it.");
        return "";
    }

    my($line,$callback)=@_;
    $SIG{CHLD}="IGNORE";
    my $pid=eval { fork(); };         # Don't worry if OS isn't forking
    return 'NOREPLY' if $pid;
    $callback->(&earthquake_getdata($line));
    if (defined($pid))                # child exits, non-forking OS returns
    {
        exit 0 if ($no_posix);
        POSIX::_exit(0);
    }
}

sub earthquake_getdata($)
{
	my $option=shift;
	my $url;
	my $result;

	print Dumper($option);

		if ($option eq "sighour") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_hour.atom";
		}
		elsif ($option eq "4.5hour") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_hour.atom";
		}
		elsif ($option eq "2.5hour") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_hour.atom";
		}
		elsif ($option eq "1.0hour") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_hour.atom";
		}
		elsif ($option eq "allhour") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_hour.atom";
		}
		elsif ($option eq "sigday") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_day.atom";
		}
		elsif ($option eq "4.5day") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_day.atom";
		}
		elsif ($option eq "2.5day") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_day.atom";
		}
		elsif ($option eq "1.0day") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_day.atom";
		}
		elsif ($option eq "allday") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.atom";
		}
		elsif ($option eq "sigweek") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_week.atom";
		}
		elsif ($option eq "4.5week") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_week.atom";
		}
		elsif ($option eq "2.5week") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_week.atom";
		}
		elsif ($option eq "1.0week") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_week.atom";
		}
		elsif ($option eq "allweek") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_week.atom";
		}
		elsif ($option eq "sigmonth") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_month.atom";
		}
		elsif ($option eq "4.5month") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_month.atom";
		}
		elsif ($option eq "2.5month") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/2.5_month.atom";
		}
		elsif ($option eq "1.0month") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_month.atom";
		}
		elsif ($option eq "allmonth") {
			$url =  "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_month.atom";
		}
		elsif ($option eq "help") {
			$result = "The earthquake command retrieves a list of recent earthquakes from the USGS Atom feeds. The options for feeds require a magnitude and time period. For example, the default option is '4.5day'. \n";
			$result = $result . "Available magnitude choices are: all, 1.0, 2.5, 4.5, and sig. Sig means significant, which the USGS classifies as 'Earthquakes of magnitude 6.5 or greater or ones that caused fatalities, injuries or substantial damage.'\n";
			$result = $result . "Available time period choices are: hour, day, week, month. So, some example options would be 'sigweek', '2.5hour', '4.5month'. Try one!\n";
			return $result;
		}
		else {
			$result = "No option selected, using default. For a list of options, try 'earthquake help' or 'eq help'.\n";
			$url = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_day.atom";
			
		}


	my $feed = XML::Feed->parse(URI->new($url))
        or die XML::Feed->errstr;
    $result = $result . $feed->title . " -> " . $url;
	if ($url =~ /month/) {
		$result = $result . " (updated every 15 minutes)\n";
	}
	else {
		$result = $result . " (updated every 5 minutes)\n";
	}
	my $i = 0;
    for my $entry ($feed->entries) {
		if ($i > 7) {
			my $count = scalar($feed->entries);
			$result = $result . "Too many results, there are " . $count . " total in this feed. See http://earthquake.usgs.gov/earthquakes/map/ for more.";
			last;
		}
		my ($timeofquake) = $entry->summary->body =~ /<dd>(\d{4}.*UTC)/;
		
		#print Dumper($entry->summary->body);
		#print Dumper($timeofquake);
		$result = $result . $entry->title . " at " . $timeofquake . " -> " . $entry->link . "\n";
		$i++;
    }
	
	if ($i == 0) {
		$result = $result . "No current entries on this feed.";
	}
	elsif ($i < 9) {
		$result = $result . "End of list.";
	}
	return $result;
  
  
}


#------------------------------------------------------------------------
# This is the main interface to infobot
#------------------------------------------------------------------------

sub scan(&$$) {
    my ($callback, $message, $who)=@_;

	if ( $message =~ /^\s*(?:eq|earthquake).*$/i ) {
		&main::status("earthquake query");
		if ( $message =~ /^\s*(?:eq|earthquake)\s+(.+)$/i ) {
			&earthquake_get($1,$callback);
		}
		else {
			&earthquake_get("default",$callback);
		}
		return 1;
    }
}

return "earthquake";
