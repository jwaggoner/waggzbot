#------------------------------------------------------------------------
# NOAA Weather module.
#
# kevin lenzo (C) 1999 -- get the weather forcast NOAA.
# feel free to use, copy, cut up, and modify, but if
# you do something cool with it, let me know.
#
# $Id: weather.pm,v 1.11 2005/01/21 20:58:03 rich_lafferty Exp $
# Update 2015-05-21_v2 waggz
#------------------------------------------------------------------------

package weather;

my $no_weather;
my $default = 'KAGC';

BEGIN {
    $no_weather = 0;
    eval "use LWP::UserAgent";
	eval "use Geo::WeatherNWS";
    eval "use Data::Dumper";
	eval "use POSIX qw(strftime)";
	eval "use Sys::Info";
	eval "use Sys::Info::Constants qw( :device_cpu )";
	eval "my $info = Sys::Info->new";
	eval "my $cpu  = $info->device( CPU => %options )";
	eval "use Sys::Info::Constants qw( :device_cpu )";
	eval "use Sys::MemInfo qw(totalmem freemem totalswap)";
	eval "use JSON qw( decode_json )";
	eval "use LWP::Simple";
	eval "use Data::Dumper";
    $no_weather++ if ($@);
}

sub get_weather {
    my ($station) = shift;
    my $result;

    # make this work like Aviation
    $station = uc($station);
	
	#allow custom preset responses
	my %presets;
	my $now_string = strftime "%b %e %Y %H:00 UTC", gmtime;
	our %options;
	my $info = Sys::Info->new;
	my $cpu  = $info->device( CPU => %options );
	my $os   = $info->os(%options);
	my $up = $os->uptime;
	my $tick = $os->tick_count;
	my $myhostname = `uname -n`;
	chomp $myhostname;
	
	
	$presets{'HELL'} = "Hell, Under Ground; (HELL) 00-00N 000-0W 0M; last updated: $now_string; Dew Point: 440 F (370 C); Pressure (altimeter): 300.53 in. Hg (10340 hPa); Pressure tendency: 0.01 inches (0.4 hPa) higher than three hours ago; Relative Humidity: 0.0%; Sky conditions: N/A ; Temperature: 460 F (380 C); Visibility: 5 mile(s); Weather: Hot; Wind: from Satan (280 degrees) at 7 MPH (6 KT); Winchill: 170 f (120 C)";
	$presets{'ISSS'} = "ISS, International Space Station. Lat., Long. varies; last updated 5 minutes ago; Pressure (altimeter) 0.00in. Hg (0 hPa); Pressure tendency: 0.00 in (0.0 hPa) higher than 3 hours ago; Relative Humidity: 0%; Sky conditions: clear, black; Temperature: 3K; Visibility: 13.7Bn Lightyear(s); Weather: N/A; Wind: Solar, light, from: heliocentric; Windchill: N/A;";
	$presets{'FUCK'} = "Well, fuck you too, fag!";
	$presets{'ACME'} = sprintf "My server's name is %s and is running %s, the current CPU load is %s, memory is %d MB free of %d MB total, and was last booted %s (up %.2f days)\n", $myhostname, $os->name( long => 1 ), $cpu->load, &freemem / 1048576, &totalmem / 1048576, scalar localtime $up, $tick / (60*60*24   );
	$presets{'HEAVEN'} = "ERROR: request timed out";
	$presets{'SUCKS'} = "but you swallow!";
	$presets{'ZEROCOOL'} = "If he has zero cool wouldn't he be burning hot?";

	if (exists $presets{$station}) {
		return $presets{$station};
	}
    
    my $station = uc($2);
    $station =~ s/[.?!]$//;
    $station =~ s/\s+$//g;
    return "'$station' doesn't look like a valid ICAO airport identifier."
        unless $station =~ /^[\w\d]{3,4}$/;
    $station = "C" . $station if length($station) == 3 && $station =~ /^Y/;
    $station = "K" . $station if length($station) == 3;

	
    if ($no_weather) {
        return 0;
    }
	else {
		my $Report=Geo::WeatherNWS::new();
		$Report->getreport($station);
		if ($Report->{error}) {
			$result = "Error: $Report->{errortext} Please check that you have a valid ICAO identifier. You can search by typing 'airport code for <city>' or get more information by typing 'airport name for <code>'.";
		}
		else {
			my $file = "nsd_cccc.txt";
			my $document = do {
				local $/ = undef;
				open my $fh, "<", $file
				or die "could not open $file: $!";
				<$fh>;
			};
			my $stationname;
			if ($document =~ /(^$station;.*$)/imx) {
				my @values = split(';', $1);
				$stationname = "$values[3]";
				if ($values[4] ne "") {
					$stationname = $stationname . " $values[4]";
				}
				elsif ($values[5] ne "") {
					$stationname = $stationname . " $values[5]";
				}
			}

	
			
			$result = "Weather for $Report->{code}";
			if ($stationname ne "") {
				$result = $result . " $stationname";
			}

			$result = $result . " as of";
			my $suffix;
			if    ($Report->{day} =~ /(?<!1)1$/) { $suffix = 'st'; }
			elsif ($Report->{day} =~ /(?<!1)2$/) { $suffix = 'nd'; }
			elsif ($Report->{day} =~ /(?<!1)3$/) { $suffix = 'rd'; }
			else 			{ $suffix = 'th'; }

			$result = $result . " $Report->{time} UTC on the $Report->{day}$suffix:";
			
			if ($Report->{conditionstext} ne "") {
				$result = $result . " $Report->{conditionstext}";
			}
			
			$result = $result . " $Report->{temperature_f}F ($Report->{temperature_c}C)";

			if ($Report->{windspeedmph} ne "") {
				$result = $result . " with winds from the $Report->{winddirtext} ($Report->{winddir}) at $Report->{windspeedmph} mph ($Report->{windspeedkts} kts)";
			}
			
			if ($Report->{windgustmph} != 0) {
				$result = $result . "gusting to $Report->{windgustmph} mph ($Report->{windgustkts} kts).";
			}
			else {
				$result = $result . ".";
			}
			
			if ($Report->{relative_humidity} ne "") {
				$result = $result . " Relative humidity: $Report->{relative_humidity}%.";
			}
			
			if ($Report->{dewpoint_f} ne "") {
				$result = $result . " Dewpoint: $Report->{dewpoint_f}F ($Report->{dewpoint_c}C).";
			}
			if ($Report->{heat_index_f} ne "" and $Report->{temperature_f} > 80) {
				$result = $result . " Heat index: $Report->{heat_index_f}F ($Report->{heat_index_c}C).";
			}
			
			if ($Report->{windchill_f} ne "" and $Report->{temperature_f} < 50) {
				$result = $result . " Wind chill: $Report->{windchill_f}F ($Report->{windchill_c}C).";
			}
			
			if ($Report->{visibility_mi} ne "") {
				$result = $result . " Visibility: $Report->{visibility_mi} miles ($Report->{visibility_km} km).";
			}
			
			if ($Report->{cloudcover} ne "") {
				$result = $result . " Clouds: $Report->{cloudcover}.";
			}
			
			if ($Report->{pressure_inhg} ne "") {
				$result = $result . " Air Pressure: $Report->{pressure_inhg} in ($Report->{pressure_mb} mb).";
			}
		
			
		}
	
        return $result;
    }
}

sub get_forecast {
    my ($station) = shift;
    my $result;
	my $who=shift;

	#do geolookup
	my $geourl = sprintf "http://api.wunderground.com/api/a922131adb535458/geolookup/q/%s.json", $station;
	my $geolookup = get ($geourl);
	return "Could not get $geourl!" unless defined $geolookup;
	my $georesponse = decode_json( $geolookup );

	#check for request url
	if ($georesponse->{'response'}{'error'}) {
		$result = $georesponse->{'response'}{'error'}{'description'};
	}
	else {
		my $requesturl = $georesponse->{'location'}{'requesturl'};
		if ($requesturl) {
			$result = "https://www.wunderground.com/" . $requesturl;
			::msg($who,$result);
			my $needle = "global";
			if (substr($requesturl, 0, length($needle)) eq $needle) {
				$requesturl = $georesponse->{'location'}{'nearby_weather_stations'}{'airport'}{'station'}[0]{'icao'} . ".json";
			}

			my $forecasturl = "http://api.wunderground.com/api/a922131adb535458/forecast/q/" . $requesturl;
			$forecasturl =~ s/.html/.json/g;
			my $forecastjson = get ($forecasturl);
			my $forecast = decode_json ($forecastjson);
			my $txtfroecast = $forecast->{'forecast'};
			#::msg($who,$txtforecast);
			my $forecasts = $forecast->{'forecast'}{'txt_forecast'}{'forecastday'};
			foreach my $record (@$forecasts)
			{
				#printf "Period: %s\n", $record->{'period'};
				my $txtforecastperiod = sprintf "%s: %s\n", $record->{'title'}, $record->{'fcttext'};
				::msg($who,$txtforecastperiod);
			}
			undef $forecasts;
		}
		else {
			$result = "Too many results, please be more specific. You can add a state or country to your search.";
		}
	}
	#if yes, get forecast from request url
	
	
	#else return error
	return $result;
}


sub scan (&$$) {
    my ($callback,$message,$who) = @_;

    if (::getparam('weather') 
            and ($message =~ /^\s*(wx|weather)\s+(?:for\s+)?(.*?)\s*\?*\s*$/)) {
        my $code = $2;
        $callback->(get_weather($code));
        return 'NOREPLY';
    }
    if (::getparam('weather') 
            and ($message =~ /^\s*(forecast)\s+(?:for\s+)?(.*?)\s*\?*\s*$/)) {
        my $code = $2;
        $callback->(get_forecast($code, $who));
        return 'NOREPLY';
    }
    return undef;
}


"weather";

__END__

=head1 NAME

weather.pm - Get the weather from a NOAA server

=head1 PREREQUISITES

	LWP::UserAgent

=head1 PARAMETERS

weather

=head1 PUBLIC INTERFACE

	weather [for] <station>

=head1 DESCRIPTION

Contacts C<weather.noaa.gov> and gets the weather report for a given
station.

=head1 AUTHORS

Kevin Lenzo
