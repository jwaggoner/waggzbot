
package q2rcon;

use strict;
use warnings;


my ($no_stuff, $no_posix);
our $okay;

BEGIN {
    eval qq{
		use Data::Dumper;
		use Parallel::ForkManager;
    };
    $no_stuff++ if ($@);

    eval qq{
        use POSIX;
    };
    $no_posix++ if ($@);
	$okay = 0;
}


sub start_fork($$)
{
    if($no_stuff)
    {
        &main::status("Sorry, Q2RCON.pm requires STUFF.");
        return "";
    }
	print "test1\n";
	if ($okay) {
		$okay = 0;
		print "okay = false\n";
	}
	else {
		$okay = 1;
		print "okay = true\n";
	}
    my($line,$callback)=@_;
    $SIG{CHLD}="IGNORE";
    my $pid=eval { fork(); };         # Don't worry if OS isn't forking
    return 'NOREPLY' if $pid;
    $callback->(&fork_rcon($line));
    if (defined($pid))                # child exits, non-forking OS returns
    {
        exit 0 if ($no_posix);
        POSIX::_exit(0);
    }
	print "test2\n";
}

sub fork_rcon {
	print "test3\n";
my $max_procs = 1;
# hash to resolve PID's back to child specific information
 
my $pm = Parallel::ForkManager->new($max_procs);
 
# Setup a callback for when a child finishes up so we can
# get it's exit code
$pm->run_on_finish( sub {
    my ($pid, $exit_code, $ident) = @_;
    print "** $ident just got out of the pool ".
      "with PID $pid and exit code: $exit_code\n";
});
 
$pm->run_on_start( sub {
    my ($pid, $ident)=@_;
    print "** $ident started, pid: $pid\n";
	do_rcon();
	sleep 10;
});
 
$pm->run_on_wait( sub {
    print "** Have to wait for one children ...\n"
  },
  0.5
);
 
while ($okay) {
  my $pid = $pm->start("test1");
 
  # This code is the child process
  #print "This is $names[$child], Child number $child\n";
  #sleep ( 2 * $child );
  #print "$names[$child], Child $child is about to get out...\n";
  sleep 11;
  $pm->finish("test1"); # pass an exit code to finish
  #rawout("PRIVMSG #waggzbot testing");
  print "test7\n";
  	if ($okay) {
		print "okay = true\n";
	}
	else {
		print "okay = false\n";
		last;
	}
}

}


sub do_rcon() {

	my $result = `perl ./kkrcon/kkrcon.pl -a joshw.serveftp.com -p 27910 -t old 123456abcdef status`;

	my @players = $result =~ /^.{15}(.{15}.{9}\d.*?)\s.*?$/img;
	
	print Dumper(@players);
	
	
	
	#splice(@players,0,2);
	$result = "Player list: \n";
	for my $playername (@players) {
		#print "playername1: " . Dumper($playername);
		my ($player) = $playername =~ /^(.{15}).*?$/;
		#print "playername2: " . Dumper($playername);
		my ($ip) = $playername =~ /^.{15}.{9}(.*)$/;
		#print "playername3: " . Dumper($playername);
		print Dumper($player);
		print Dumper($ip);
		$result = $result . $player . " " . $ip . "\n";
	}
	&main::say ($result);
	#return $result;
	print "test8\n";
	if ($okay) {
		print "okay = true\n";
	}
	else {
		print "okay = false\n";
	}
}

sub scan(&$$) {
    my ($callback,$message,$who) = @_;

    if($message=~/^\s*testrcon\s*$/) {
		print "test4";
		&start_fork($callback);
		print "test5";
        return 1;
    } else {
		print "test6";
        return undef;
    }
}

return "q2rcon";
